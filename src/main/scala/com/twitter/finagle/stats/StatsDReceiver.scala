package com.twitter.finagle.stats

import java.net.{DatagramPacket, InetSocketAddress, InetAddress, DatagramSocket}
import com.twitter.finagle.{stats, Codec, CodecFactory}

class StatsDReceiver(host: String, port: Integer) extends StatsReceiver {

  val socket = new DatagramSocket()

  def counter(name: String*) = new Counter {
    override def incr(delta: Int): Unit = {
      val data = (formatName(name) + ":" + delta + "|c").getBytes("UTF-8")
      socket.send(new DatagramPacket(data, 0, data.length, InetAddress.getByName(host), port))
    }
  }

  private[this] def formatName(description: Seq[String]) = {
    description mkString "/"
  }

  override def stat(name: String*) = new Stat {
    override def add(value: Float) = {
      val data = (formatName(name) + ":" + value + "|s").getBytes("UTF-8")
      socket.send(new DatagramPacket(data, data.length, InetAddress.getByName(host), port))
    }
  }

  override val repr: AnyRef = this

  override def addGauge(name: String*)(f: => Float) = new Gauge {
    override def remove(): Unit = {

    }
    val data = (formatName(name) + ":" + f + "|g").getBytes("UTF-8")
    socket.send(new DatagramPacket(data, data.length, InetAddress.getByName(host), port))
  }
}
