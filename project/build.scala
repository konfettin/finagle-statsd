import sbt._
import sbt.Keys._
object Finch extends Build {
  val baseSettings = Defaults.defaultSettings ++ Seq(
    libraryDependencies ++= Seq(
      "com.twitter" %% "finagle-core" % "6.18.0",
      "com.twitter" %% "finagle-stats" % "6.18.0"
    ),
    scalacOptions ++= Seq( "-unchecked", "-deprecation", "-feature")
  )

  lazy val buildSettings = Seq(
    organization := "com.twitter",
    version := "0.0.1",
    scalaVersion := "2.10.3"
  )
  lazy val publishSettings = Seq(
    publishMavenStyle := true,
    publishArtifact := true,
    publishTo := Some(Resolver.file("localDirectory", file(Path.userHome.absolutePath + "/repo"))),
    licenses := Seq("Apache 2.0" -> url("http://www.apache.org/licenses/LICENSE-2.0")),
    homepage := Some(url("https://github.com/konfettin/finagle-statsd")),
    pomExtra := (
      <scm>
        <url>git://github.com/konfettin/finagle-statsd.git</url>
        <connection>scm:git://github.com/konfettin/finagle-statsd.git</connection>
      </scm>
      <developers>
        <developer>
          <id>YuryShatilin</id>
          <name>Yury Shatilin</name>
          <url>https://github.com/YuryShatilin</url>
        </developer>
      </developers>
    )
  )

lazy val root = Project(id = "finagle-statsd",
  base = file("."),
  settings = baseSettings ++ buildSettings ++ publishSettings)
}
